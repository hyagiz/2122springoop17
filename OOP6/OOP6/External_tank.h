#pragma once
#include <iostream>
#include <iterator>  
#include <list>
#include <ctime>
#include "Tank.h"
#include "Engine.h"


class External_tank :public Tank
{
private:
	std::list<int>::iterator l_front;
	list<int> Temp_connectedTankid;
	int tank_id = 1, size = 100, valve_id = 1;
	list<Tank> tank_;
	observerinformation *O = observerinformation::get_observerinformation();
public:
	Engine *engine = Engine::get_Eng(); //this line is for singelton
	void AddFuelTank(string capacity, ofstream &offile);
	void RemoveFuelTank(string tank_id, ofstream &offile);
	void ListFuelTanks(ofstream &offile);
	void open_valve(string tank_id, ofstream &offile);
	void close_valve(string tank_id, ofstream &offile);
	void break_fuel_tank(string tank_id, ofstream &offile);
	void repair_fuel_tank(string tank_id, ofstream &offile);
	void printFuelTankCount(ofstream &offile);
	void printTankInfo(string tank_id, ofstream &offile);
	void listConnectedTanks(ofstream &offile);
	void printTotalFuelQuantity(ofstream &offile);
	void fillTank(string tank_id, double fuelQuantity, ofstream &offile);
	void ConnectFuelTankToEngine(string tank_id, ofstream &offile);
	void DisConnectFuelTankToEngine(string tank_id, ofstream &offile);
	void waitSecond(string second, ofstream &offile);

	~External_tank() {
		tank_.clear();
	}
};