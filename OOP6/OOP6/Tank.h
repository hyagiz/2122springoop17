#pragma once
#include<iostream>
#include "observerinformation.h"
#include "Valve.h"
class Tank
{
private:
	
	bool isconnect;
	bool isbroken;
public:
	Valve v;
	int id;
	double fuel_quantity;
	double fuel_capacity;
	Tank() {};
	
	bool get_isconnect() { return isconnect; }
	void set_isconnect(bool _isconnect) { isconnect = _isconnect; }
	bool get_isbroken() { return isbroken; }
	void set_isbroken(bool _isbroken) { isbroken = _isbroken; }
	bool get_isopen_valve() { return v.get_isopen(); }
	void set_isopen_valve(bool _isopen_valve) { v.set_isopen(_isopen_valve); }

	~Tank() {
	}
};