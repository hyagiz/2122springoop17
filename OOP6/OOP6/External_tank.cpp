﻿#include "External_tank.h"
#include <string.h>
#include <string>
#include <fstream>
#include <list>
#include <string_view>
#include <numeric>

using namespace std;

void External_tank::AddFuelTank(string capacity, ofstream &offile) {
	Tank tank;

	tank.id = tank_id;
	tank.v.set_id(valve_id);
	tank.fuel_capacity = stoi(capacity);
	tank.fuel_quantity = stoi(capacity);   //dolu tank eklendi
	tank.set_isopen_valve(0);
	tank.set_isbroken(0);
	tank.set_isconnect(0);
	O->addobs("Tank", tank_id);
	O->addobs("Valve", tank_id);
	tank_id++;
	valve_id++;
	tank_.push_back(tank);
	offile << capacity << "L capacity fuel tank is added." << endl;
	waitSecond("1", offile);
}

void External_tank::RemoveFuelTank(string tank_id, ofstream &offile) {
	try {
		int flag = 0;			//flag =0 kalırsa o id ile tank yok demek....
		list<Tank>::iterator A = tank_.begin();
		for (auto it = tank_.begin(); it != tank_.end(); ++it) {
			advance(A, 1);
			if (it->id == stoi(tank_id)) {
				if (it->get_isopen_valve() == false) {
					offile << "Tank number " << tank_id << " is removed." << endl;
					O->delobs("Valve", it->id);
					O->delobs("Tank", it->id);
					tank_.erase(it, A);
					flag = 1;
					break;
				}
				else {
					flag = 1;
					throw 10;
				}
			}
		}
		if (flag == 0) {
			throw 10.5;
		}
	}
	catch (int error11) {
		offile << "Error: You can't remove the tank when the valve is open!" << endl;
	}
	catch (double error54) {
		offile << "Error: There is no tank with this id" << endl;
	}
	waitSecond("1", offile);
}

void External_tank::ListFuelTanks(ofstream &offile) {
	for (auto it = tank_.begin(); it != tank_.end(); ++it) {
		offile << "Tank id: " << it->id << "    ";
	}
	offile << endl;
	waitSecond("1", offile);
}

void External_tank::open_valve(string tank_id, ofstream &offile) {
	try {
		int flag = 0;		//tank yok bu id ile
		for (auto it = tank_.begin(); it != tank_.end(); ++it) {

			if (it->id == stoi(tank_id)) {
				if (it->get_isconnect() == true) {
					if (it->get_isopen_valve() == false) {
						it->set_isopen_valve(true);
						flag = 1;
						offile << "The fuel tank " << tank_id << "'s valve is open." << endl;
					}
					else {
						it->set_isopen_valve(true);
						flag = 1;
						throw 11;
					}
				}
				else {
					throw 10.5;
					flag = 1;
				}
			}
		}
		if (flag == 0) {
			throw "hata10";
		}
	}
	catch (int error12) {
		offile << "Error: The fuel tank " << tank_id << "'s valve is already opened." << endl;
	}
	catch (double error13) {
		offile << "Error: The fuel tank " << tank_id << " is not connected to engine!" << endl;
	}
	catch (const char *error14) {
		offile << "Error: There is no tank with this id" << endl;
	}
	waitSecond("1", offile);
}

void External_tank::close_valve(string tank_id, ofstream &offile) {
	try {
		int flag = 0;		//tank yok bu id ile
		for (auto it = tank_.begin(); it != tank_.end(); ++it) {

			if (it->id == stoi(tank_id)) {
				if (it->get_isconnect() == true) {
					if (it->get_isopen_valve() == true) {
						it->set_isopen_valve(false);
						flag = 1;
						offile << "The fuel tank " << tank_id << "'s valve is closed." << endl;
					}
					else {
						it->set_isopen_valve(false);
						flag = 1;
						throw 12;
					}
				}
				else {
					flag = 1;
					throw 12.5;
				}
			}
		}
		if (flag == 0) {
			throw "hata12";
		}
	}
	catch (int error15) {
		offile << "Error: The fuel tank " << tank_id << "'s valve is already closed." << endl;
	}
	catch (double error16) {
		offile << "Error: The fuel tank " << tank_id << " is not connected to engine!" << endl;
	}
	catch (const char *error17) {
		offile << "Error: There is no tank with this id" << endl;
	}
	waitSecond("1", offile);
}

void External_tank::break_fuel_tank(string tank_id, ofstream &offile) {
	try {
		int flag = 0;		//tank yok bu id ile
		for (auto it = tank_.begin(); it != tank_.end(); ++it) {
			if (it->id == stoi(tank_id)) {
				if (it->get_isbroken() == false) {
					it->set_isbroken(true);
					flag = 1;
					offile << "The fuel tank " << tank_id << " is broken!" << endl;
				}
				else {
					flag = 1;
					throw 13;
				}
			}
		}
		if (flag == 0) {
			throw 13.5;
			offile << "There is no tank with this id" << endl;
		}
	}
	catch (int error18) {
		offile << "Error: The fuel tank " << tank_id << " is already broken!" << endl;
	}
	catch (double error19) {
		offile << "Error: There is no tank with this id" << endl;
	}
	waitSecond("1", offile);
}

void External_tank::repair_fuel_tank(string tank_id, ofstream &offile) {
	try {
		int flag = 0;		//tank yok bu id ile
		for (auto it = tank_.begin(); it != tank_.end(); ++it) {
			if (it->id == stoi(tank_id)) {
				if (it->get_isbroken() == true) {
					it->set_isbroken(false);
					flag = 1;
					offile << "The fuel tank " << tank_id << " is repaired!" << endl;
				}
				else {
					flag = 1;
					throw 14;
				}
			}
		}
		if (flag == 0) {
			throw 14.5;
		}
	}
	catch (int error18) {
		offile << "Error: The fuel tank " << tank_id << " is already repaired!" << endl;
	}
	catch (double error19) {
		offile << "Error: There is no tank with this id" << endl;
	}
	waitSecond("1", offile);
}

void External_tank::printFuelTankCount(ofstream &offile) {
	offile << "The number of Tanks is : " << tank_.size() << endl;
	waitSecond("1", offile);
}

void External_tank::printTankInfo(string tank_id, ofstream &offile) {
	int flag = 0;		//tank yok bu id ile
	for (auto it = tank_.begin(); it != tank_.end(); ++it) {
		if (it->id == stoi(tank_id)) {
			offile << "Tank id: " << it->id << endl;
			offile << "Tank's capacity: " << it->fuel_capacity << endl;
			offile << "Tank's fuel quantity: " << it->fuel_quantity << endl;
			it->get_isbroken() == true ? "Tank is broken" : "Tank is not broken";
			it->get_isconnect() == true ? "Tank is connected" : "Tank is not connected";
			it->get_isopen_valve() == true ? "Tank's valve is open" : "Tank's valve is not open";
			flag = 1;
		}
	}
	if (flag == 0)
		offile << "Error: There is no tank with this id" << endl;
	waitSecond("1", offile);
}

void External_tank::listConnectedTanks(ofstream &offile) {
	int flag = 0;		//tank yok bu id ile
	offile << "Connected Tank's ids : ";
	for (auto it = tank_.begin(); it != tank_.end(); ++it) {
		if (it->get_isconnect() == true) {
			offile << it->id << "	";
		}
	}
	offile << endl;
	waitSecond("1", offile);
}

void External_tank::printTotalFuelQuantity(ofstream &offile) {
	int total = 0;
	for (auto it = tank_.begin(); it != tank_.end(); ++it) {
		total = total + it->fuel_quantity;
	}
	offile << "Total fuel quantity is : " << total << endl;
	waitSecond("1", offile);
}

void External_tank::fillTank(string tank_id, double fuelQuantity, ofstream &offile) {
	int flag = 0;		//tank yok bu id ile
	for (auto it = tank_.begin(); it != tank_.end(); ++it) {
		if (it->id == stoi(tank_id)) {
			it->fuel_quantity += fuelQuantity;
			if (it->fuel_quantity > fuel_capacity)		//kapasitesinden �ok yak�t koyarsa 
				it->fuel_quantity = it->fuel_capacity; // kapasitesi kadar yak�t al�r
			flag = 1;
			offile << "Tank " << tank_id << " is filled. Its new fuel quantity is " << it->fuel_quantity << "." << endl;
		}
	}
	try {
		if (flag == 0) {
			throw 16;
		}
	}
	catch (int error21) {
		offile << "Error: There is no tank with this id" << endl;
	}
	waitSecond("1", offile);
}

void External_tank::ConnectFuelTankToEngine(string tank_id, ofstream &offile) {
	try {
		int flag = 0;		//tank yok bu id ile
		for (auto it = tank_.begin(); it != tank_.end(); ++it) {

			if (it->id == stoi(tank_id)) {
				if (it->get_isconnect() == false) {
					it->set_isconnect(true);
					flag = 1;
					engine->connectedTankid.push_back(stoi(tank_id));		//engine içindeki bağlı tanklar listesine id'yi ekledik
					offile << "The fuel tank " << tank_id << " is connected to the engine." << endl;
				}
				else {
					flag = 1;
					throw 17;
				}
			}
		}
		if (flag == 0) {
			throw 17.5;
		}
	}
	catch (int error22) {
		offile << "Error: Tank is already connected to the engine!" << endl;
	}
	catch (double error23) {
		offile << "Error: There is no tank with this id." << endl;
	}
	waitSecond("1", offile);
}

void External_tank::DisConnectFuelTankToEngine(string tank_id, ofstream &offile) {
	try {
		int flag = 0;		//tank yok bu id ile
		for (auto it = tank_.begin(); it != tank_.end(); ++it) {
			if (it->id == stoi(tank_id)) {
				if (it->get_isconnect() == false) {
					flag = 1;
					throw 18;
				}
				else {
					it->set_isconnect(false);
					flag = 1;
					for (auto it = engine->connectedTankid.begin(); it != engine->connectedTankid.end(); ++it) {
						if (*it == stoi(tank_id)) {
							engine->connectedTankid.remove(*it);
							break;
						}
					}//silme işlemi burada bitti....
					offile << "The fuel tank " << tank_id << " is disconnected. " << endl;
				}
			}
		}
		if (flag == 0) {
			throw 18.5;
		}
	}
	catch (int error22) {
		offile << "Error: Tank is already disconnected! " << endl;
	}
	catch (double error23) {
		offile << "Error: There is no tank with this id." << endl;
	}
	waitSecond("1", offile);
}

void External_tank::waitSecond(string second, ofstream &offile) {
	int random_sayi;
	int control = 1;
	srand(time(0));
	if (second != "1") {
		offile << "You are in wait mode for " << second << " seconds." << endl;
	}
	if (engine->get_status() == true)
	{
		if (engine->i.fuel_quantity < 20) {
			Temp_connectedTankid = engine->connectedTankid;
			while (control) {
				random_sayi = rand() % ((Temp_connectedTankid.size() - 1) + 1);//random say� �ret
				l_front = Temp_connectedTankid.begin();

				std::advance(l_front, random_sayi);
				for (auto it = tank_.begin(); it != tank_.end(); ++it) {
					if (it->id == *l_front)//liste i�inde o tank id'sine sahip tank� bulduk
					{
						if (it->get_isopen_valve() == true)
						{
							if (it->fuel_quantity >= engine->i.fuel_capacity - engine->i.fuel_quantity) {
								it->fuel_quantity = it->fuel_quantity - (engine->i.fuel_capacity - engine->i.fuel_quantity);
								engine->i.fuel_quantity = engine->i.fuel_capacity;//internal full oldu
								control = 0;
								break;
							}
							else if (it->fuel_quantity + engine->i.fuel_quantity == 20) {
								engine->i.fuel_quantity += it->fuel_quantity;
								it->fuel_quantity = 0;
								control = 0;
								break;
							}
						}
						else
						{
							Temp_connectedTankid.remove(*l_front);
							break;
						}
					}
					if (Temp_connectedTankid.size() == 0)
						break;

				}
				if (Temp_connectedTankid.size() == 0)
					control = 0;
			}
		}
		if (engine->get_is_full_throttle() == true)
		{
			if (engine->i.fuel_quantity - engine->get_fuel_per_second() * stoi(second) < 0) {
				offile << "engine can't consume fuel so it stopped." << endl;
				engine->set_status(false);
			}
			else
			{
				engine->i.fuel_quantity = engine->i.fuel_quantity - engine->get_fuel_per_second() * stoi(second);
				engine->set_total_consumed_fuel_quantity(engine->get_total_consumed_fuel_quantity() + stoi(second) * engine->get_fuel_per_second());
				engine->set_FullTHSECOND(engine->get_FullTHSECOND() - stoi(second));
				if (engine->get_FullTHSECOND() == 0)
					engine->set_is_full_throttle(false);
			}

		}
		else
		{
			if (engine->i.fuel_quantity - engine->get_fuel_per_second() * stoi(second) > 0) {
				engine->i.fuel_quantity = engine->i.fuel_quantity - engine->get_fuel_per_second() * stoi(second);
				engine->set_total_consumed_fuel_quantity(engine->get_total_consumed_fuel_quantity() + stoi(second) * engine->get_fuel_per_second());
			}
			else
			{
				offile << "engine can't consume fuel so it stopped." << endl;
				engine->set_status(false);
			}
		}
		if (engine->i.fuel_quantity <= 0) {
			engine->i.fuel_quantity = 0;
			engine->set_status(false);
		}
	}
	if (engine->get_status() == true)
	{
		if (engine->get_is_full_throttle() == true)
		{
			engine->set_engineHeat(engine->get_engineHeat() + 5 * stoi(second));
			if (engine->get_engineHeat() < 90)
				engine->set_engineHealthPercent(engine->get_engineHealthPercent() - stoi(second));
		}
		else
		{
			if (engine->get_engineHeat() > 90)
				engine->set_engineHeat(engine->get_engineHeat() - 1 * stoi(second));
			else
				engine->set_engineHeat(engine->get_engineHeat() + 1 * stoi(second));
		}
	}
	else
	{
		if (engine->get_engineHeat() > 20)
			engine->set_engineHeat(engine->get_engineHeat() - 1 * stoi(second));
	}
	if (engine->get_engineHeat() > 130)
		engine->set_engineHealthPercent(engine->get_engineHealthPercent() - stoi(second));
}

External_tank operator +(class External_tank tank1, class External_tank tank2) {
	External_tank temp;
	temp.fuel_quantity = tank1.fuel_quantity + tank2.fuel_quantity;
	tank1.fuel_quantity = 0;//to drain tanks I havent written const class
	tank2.fuel_quantity = 0;
	return temp;
}