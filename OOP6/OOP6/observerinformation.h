#pragma once
#include <list>
#include <string>
#include <iostream>
#include "observer.h"
#include <fstream>

using namespace std;
class observerinformation
{
private :
	static observerinformation *O;
public:
	observer *obs;
	list<observer*> o;
	observerinformation() {};
	static observerinformation* get_observerinformation() {
		if (O == NULL)
			O = new observerinformation();
		return (O);
	
	}
	void addobs(string thing, int id)
	{
		obs = new observer(thing, id);
		o.push_back(obs);
	}

	void delobs(string thing, int id) {
		list<observer*> x;

		for (auto it = o.begin(); it != o.end(); it++){
			if ((*it)->getid() == id && (*it)->getwhat() == thing) {
			}
			else
			{
				x.push_back(*it);
			}
		}
		o = x;
	}
		
	void get_information(ofstream &offile)
	{
		for (auto it = o.begin(); it != o.end(); it++) {
			if ((*it)->getwhat() == "Engine")
				offile << "Engine: Simulation stopped" << endl;
			else if ((*it)->getwhat() == "Tank") {
				offile << "Tank " << (*it)->getid() << " : Simulation stopped" << endl;
			}
			else if ((*it)->getwhat() == "Valve") {
				offile << "Valve " << (*it)->getid() << " : Simulation stopped" << endl;
			}
		}
	}
	~observerinformation() {
	};
};


