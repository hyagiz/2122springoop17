#include "Engine.h"

void Engine::EngineStart(ofstream &offile) {
	engineHeat = 90;
	try {
		if (engineHealthPercent > 20) {
			if (connectedTankid.size() != 0) {
				if (status == false) {
					status = true;
					offile << "Engine is started." << endl;
					O->addobs("Engine", 1);
				}
				else {
					throw 1;
				}
			}
			else {
				throw 1.5;
			}
		}
		else {
			throw "hata";
		}
	}
	catch (int error1) {
		offile << "Error: Engine is running already!" << endl;
	}
	catch (double error2) {
		offile << "Error: There aren't connected tanks!" << endl;
	}
	catch (const char *error3) {
		offile << "Error: Engine's health is low!" << endl;
	}
}

void Engine::EngineStop(ofstream &offile) {
	engineHeat = 20;
	try {
		if (status == true) {
			status = false;
			offile << "Engine is stopped." << endl;
			O->delobs("Engine", 1);
		}
		else {
			throw 2;
		}
	}
	catch (int error4) {
		offile << "Error: Engine isn't working already!" << endl;
	}
}

void Engine::repair_engine(ofstream &offile) {
	try {
		if (status == true) {
			throw 3;
		}
		else {
			if (engineHealthPercent != 0) {
				if (isBroken == false) {
					throw 2.5;
				}
				else {
					offile << "The engine has been repaired." << endl;
					isBroken = false;
				}
			}
			else {
				throw "hata2";
			}
		}
	}
	catch (int error5) {
		offile << "Error: Engine is running. You can't repair while it is running." << endl;
	}
	catch (double error6) {
		offile << "Error: Engine is not broken." << endl;
	}
	catch (const char *error7) {
		offile << "Error: You can't repair engine when its health percent is equal to the 0" << endl;
	}
}

void Engine::change_engine_block(ofstream &offile) {
	try {
		if (status == true) {
			throw 4;
		}
		else {
			if (engineHealthPercent == 0) {			//resetting engine
				engineHeat = 20;
				is_full_throttle = false;
				isBroken = false;
				engineHealthPercent = 100;
				offile << "Engine has been changed!" << endl;
			}
			else {
				throw 3.5;
			}
		}
	}
	catch (int error8) {
		offile << "Error: Engine is running. You can't change the engine while it is running." << endl;
	}
	catch (double error9) {
		offile << "Error: You can't change the engine block when its health percent is not 0!" << endl;
	}
}

void Engine::full_throttle(string second, ofstream &offile) {
	try {
		if (status == true) {
			FullTHSECOND = stoi(second);
			//total_consumed_fuel_quantity += (5 * fuel_per_second) * FullTHSECOND;
			offile << "You are in full throttle mode for " << second << " seconds" << endl;
		}
		else {
			throw 5;
		}
	}
	catch (int error10) {
		offile << "Error: You can't start full throttle mode while engine is not running! " << endl;
	}
}

void Engine::printTotalConsumedFuelQuantity(ofstream &offile) {
	offile << "Total consumed fuel quantity is : " << total_consumed_fuel_quantity << endl;
}