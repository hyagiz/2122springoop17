#pragma once
#include <iostream>
#include <string>
using namespace std;
class observer
{
private:
	string what;
	int id;
public:
	observer(string s, int i) :what(s), id(i) {
	}
	string getwhat(){
		return what;
	}
	int getid()
	{
		return id;
	}
	~observer() {}
};

