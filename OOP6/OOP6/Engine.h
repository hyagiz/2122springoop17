#pragma once
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>  
#include <sstream>
#include <conio.h>
#include <iterator>  
#include <list>
#include "Ưnternal_tank.h"

using namespace std;

class Engine
{
private:
	observerinformation *O = observerinformation::get_observerinformation();
	static Engine *Eng;  //this line is for singelton
	Engine() {};
	double total_consumed_fuel_quantity = 0;
	int engineHeat = 20;
	bool status = false;
	bool isBroken = false;
	bool is_full_throttle = false;
	unsigned int engineHealthPercent = 100;
	const double fuel_per_second = 5.5; 
	int FullTHSECOND = 0;
public:
	static Engine* get_Eng(){
		if (Eng == NULL)
			Eng = new Engine();
		return (Eng);
	}
	Ưnternal_tank i;
	list<int> connectedTankid;			// bagli tanklar icin olusturulan liste
	void EngineStart(ofstream &offile);
	void EngineStop(ofstream &offile);
	void repair_engine(ofstream &offile);
	void change_engine_block(ofstream &offile);
	void full_throttle(string second, ofstream &offile);
	void printTotalConsumedFuelQuantity(ofstream &offile);
	
	
	//get-set functions
	int get_FullTHSECOND() { return FullTHSECOND; }
	void set_FullTHSECOND(int a) { FullTHSECOND = a; }
	const double get_fuel_per_second() { return fuel_per_second; }
	unsigned int get_engineHealthPercent() { return engineHealthPercent; }
	void set_engineHealthPercent(unsigned int s) { engineHealthPercent = s; }
	bool get_is_full_throttle() { return is_full_throttle; }
	void set_is_full_throttle(bool a) { is_full_throttle = a; }
	bool get_status() { return status; }
	void set_status(bool s) { status = s; }
	int get_engineHeat() { return  engineHeat = 20; }
	void set_engineHeat(int h) { engineHeat = h; }
	double get_total_consumed_fuel_quantity() { return total_consumed_fuel_quantity; }
	void set_total_consumed_fuel_quantity(double a) { total_consumed_fuel_quantity = a; }
	~Engine() {}//destructor
};