#pragma once
#include "Tank.h"
#include "External_tank.h"
#include "observerinformation.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <iomanip>  

using namespace std;
class input
{
private:
	External_tank externalTank;
	bool isCommandValid = false;
public:
	input() {
		observerinformation *O = observerinformation::get_observerinformation();
		ofstream offile;
		offile.open("output.txt");						//output file
		string word, word2;
		fstream file;
		file.open("User_commands.txt");					// input file
		string command, copycommand;

		while (command != "stop_simulation;")			// simulasyon durana kadar fonksiyonlar calistiriliyor
		{
			if (file.eof() == false) {
				getline(file, command);
				istringstream ss(command);

				while (ss >> word) {
					try {
						isCommandValid = false;
						if (word == "start_engine;") {
				
							externalTank.engine->EngineStart(offile);
							externalTank.waitSecond("1", offile);
							isCommandValid = true;
						}

						else if (word == "stop_engine;") {
							externalTank.engine->EngineStop(offile);
							externalTank.waitSecond("1", offile);
							isCommandValid = true;
						}

						else if (word == "add_fuel_tank") {
							ss >> word;
							word = word.substr(0, word.length() - 1);
							externalTank.AddFuelTank(word, offile);
							isCommandValid = true;
						}

						else if (word == "list_fuel_tanks;") {
							externalTank.ListFuelTanks(offile);
							isCommandValid = true;
						}

						else if (word == "remove_fuel_tank") {
							ss >> word;
							word = word.substr(0, word.length() - 1);
							externalTank.RemoveFuelTank(word, offile);
							isCommandValid = true;
						}

						else if (word == "connect_fuel_tank_to_engine") {
							ss >> word;
							word = word.substr(0, word.length() - 1);
							externalTank.ConnectFuelTankToEngine(word, offile);
							isCommandValid = true;
						}

						else if (word == "disconnect_fuel_tank_from_engine") {
							ss >> word;
							word = word.substr(0, word.length() - 1);
							externalTank.DisConnectFuelTankToEngine(word, offile);
							isCommandValid = true;
						}

						else if (word == "open_valve") {
							ss >> word;
							word = word.substr(0, word.length() - 1);
							externalTank.open_valve(word, offile);
							isCommandValid = true;
						}

						else if (word == "close_valve") {
							ss >> word;
							word = word.substr(0, word.length() - 1);
							externalTank.close_valve(word, offile);
							isCommandValid = true;
						}

						else if (word == "break_fuel_tank") {
							ss >> word;
							word = word.substr(0, word.length() - 1);
							externalTank.break_fuel_tank(word, offile);
							isCommandValid = true;
						}

						else if (word == "repair_fuel_tank") {
						ss >> word;
						word = word.substr(0, word.length() - 1);
						externalTank.repair_fuel_tank(word, offile);
						isCommandValid = true;
						}

						else if (word == "print_fuel_tank_count;") {
						ss >> word;
						word = word.substr(0, word.length() - 1);
						externalTank.printFuelTankCount(offile);
						isCommandValid = true;
						}

						else if (word == "list_connected_tanks;") {
						ss >> word;
						word = word.substr(0, word.length() - 1);
						externalTank.listConnectedTanks(offile);
						isCommandValid = true;
						}

						else if (word == "print_total_fuel_quantity;") {
						ss >> word;
						word = word.substr(0, word.length() - 1);
						externalTank.printTotalFuelQuantity(offile);
						isCommandValid = true;
						}

						else if (word == "print_total_consumed_fuel_quantity;") {
						ss >> word;
						word = word.substr(0, word.length() - 1);
						externalTank.engine->printTotalConsumedFuelQuantity(offile);
						externalTank.waitSecond("1", offile);
						isCommandValid = true;
						}

						else if (word == "print_tank_info") {
						ss >> word;
						word = word.substr(0, word.length() - 1);
						externalTank.printTankInfo(word, offile);
						isCommandValid = true;
						}

						else if (word == "fill_tank") {
						string tank_id;
						ss >> tank_id;
						ss >> word;
						word = word.substr(0, word.length() - 1);
						double capacity = stod(word);
						externalTank.fillTank(tank_id, capacity, offile);
						isCommandValid = true;
						}

						else if (word == "wait") {
						ss >> word;
						word = word.substr(0, word.length() - 1);
						externalTank.waitSecond(word, offile);
						isCommandValid = true;
						}

						else if (word == "change_engine_block;") {
						ss >> word;
						word = word.substr(0, word.length() - 1);
						externalTank.engine->change_engine_block(offile);
						externalTank.waitSecond("1", offile);
						isCommandValid = true;
						}

						else if (word == "repair_engine;") {
						ss >> word;
						word = word.substr(0, word.length() - 1);
						externalTank.engine->repair_engine(offile);
						externalTank.waitSecond("1", offile);
						isCommandValid = true;
						}

						else if (word == "full_throttle") {
						ss >> word;
						word = word.substr(0, word.length() - 1);
						externalTank.engine->full_throttle(word, offile);
						externalTank.waitSecond("1", offile);
						isCommandValid = true;
						}

						if (isCommandValid == false && command != "stop_simulation;") {
							throw string("The command doesn't exist");
						}
					}
					catch (string e) {
						offile << e << endl;
					}
				}
			}
		}
		O->get_information(offile);
	}
};